# ilmz
Import Logs MariaDB ZNC

Uhm, I'm not that good with names. 

## Why? 
Importing logs from ZNC into a database is qool and might be useful for other projects. 

## Usage
Running the `ilmz` program with `config.json` configured to your liking will output csv files that are ready to be imported into MySQL or MariaDB. See `nightly.sh` and `nightly.sql` for examples. You're gonna have to adjust things to match what servers you are on.

The log directory structure is that of ZNC.


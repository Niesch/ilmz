package main

type Config struct {
	ChannelsOnly    bool   `json:"ChannelsOnly"`
	Path            string `json:"Path"`
	Extension       string `json:"Extension"`
	OutputDirectory string `json:"OutputDirectory"`
	Debug           bool   `json:"Debug"`
}

package main

import (
	"encoding/csv"
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/Niesch/ilmz/walker"
	"log"
	"os"
)

func main() {
	conffile := flag.String("config", "config.json", "path to the json configuration file")
	flag.Parse()

	f, err := os.Open(*conffile)

	if err != nil {
		log.Fatal(err)
	}
	decoder := json.NewDecoder(f)
	conf := Config{}

	err = decoder.Decode(&conf)
	if err != nil {
		log.Fatal(err)
	}

	servers, err := walker.GetServers(conf.Path)
	if err != nil {
		log.Fatal(err)
	}

	for _, srv := range servers {
		srv.Channels, err = walker.GetChannels(srv)
		if err != nil {
			log.Fatal(err)
		}

		go srv.ParseServer()

		for _, chn := range srv.Channels {
			if (conf.ChannelsOnly && !chn.IsQuery) || !conf.ChannelsOnly {

				dfn := conf.OutputDirectory + srv.Name + ".csv"

				dfh, err := os.OpenFile(dfn, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
				if err != nil {
					log.Fatal(err)
				}

				if conf.Debug {
					log.Printf("Adding %s to %s.\n", chn.Name, dfn)
				}

				cwr := csv.NewWriter(dfh)
				err = func() error {
					for {
						select {
						case line := <-chn.Lines:
							err = cwr.Write([]string{line.Hash, line.User, line.Message, chn.Name, line.FilePath, line.Time.Format("2006-01-02 15:04:05"), fmt.Sprintf("%d", btoi(line.IsAction))})
							if err != nil {
								return err
							}
						case <-chn.Done:
							return nil
						}

					}
				}()
				if err != nil {
					log.Fatal(err)
				}
				cwr.Flush()
				if cwr.Error() != nil {
					log.Fatal(cwr.Error())
				}

				dfh.Close()
			}
		}
		if srv.Err != nil {
			log.Printf("Encountered error in parsing server %s: %s\n", srv.Name, srv.Err.Error())
		}
	}
}

func btoi(b bool) int {
	if b {
		return 1
	}
	return 0
}

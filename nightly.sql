set autocommit = 0;
set unique_checks = 0;
set names utf8mb4 collate utf8mb4_unicode_ci;

truncate table ilmz.bth;
load data local infile 'out/bth.csv' into table ilmz.bth fields terminated by ',' enclosed by '"' escaped by '' lines terminated by '\n' (@id, user, message, channel, filename, time, action) set id = unhex(@id);

truncate table ilmz.btn;
load data local infile 'out/btn.csv' into table ilmz.btn fields terminated by ',' enclosed by '"' escaped by '' lines terminated by '\n' (@id, user, message, channel, filename, time, action) set id = unhex(@id);

truncate table ilmz.freenode;
load data local infile 'out/freenode.csv' into table ilmz.freenode fields terminated by ',' enclosed by '"' escaped by '' lines terminated by '\n' (@id, user, message, channel, filename, time, action) set id = unhex(@id);

truncate table ilmz.privnet;
load data local infile 'out/privnet.csv' into table ilmz.privnet fields terminated by ',' enclosed by '"' escaped by '' lines terminated by '\n' (@id, user, message, channel, filename, time, action) set id = unhex(@id);

truncate table ilmz.rizon;
load data local infile 'out/rizon.csv' into table ilmz.rizon fields terminated by ',' enclosed by '"' escaped by '' lines terminated by '\n' (@id, user, message, channel, filename, time, action) set id = unhex(@id);

truncate table ilmz.swehack;
load data local infile 'out/swehack.csv' into table ilmz.swehack fields terminated by ',' enclosed by '"' escaped by '' lines terminated by '\n' (@id, user, message, channel, filename, time, action) set id = unhex(@id);

truncate table ilmz.wille;
load data local infile 'out/wille.csv' into table ilmz.wille fields terminated by ',' enclosed by '"' escaped by '' lines terminated by '\n' (@id, user, message, channel, filename, time, action) set id = unhex(@id);
commit;

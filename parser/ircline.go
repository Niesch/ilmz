package parser

import (
	"time"
)

// IRCServer represents an IRC Server.
type IRCServer struct {
	Name     string
	Path     string
	Channels []*IRCChannel
	Err      error
}

// IRCChannel represents an IRC Channel.
type IRCChannel struct {
	Name    string
	Path    string
	IsQuery bool
	Lines   chan *IRCLine
	Done    chan bool
}

// IRCLine represents a single line from a channel.
type IRCLine struct {
	Hash       string
	User       string
	Message    string
	FilePath   string
	Time       time.Time
	IsAction   bool
	LineNumber int
	RawLine    string
}

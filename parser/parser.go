package parser

import (
	"bufio"
	"crypto/sha256"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

// ParseServer takes an IRCServer and returns it populated.
func (srv *IRCServer) ParseServer() {
	for _, chn := range srv.Channels {
		var wg sync.WaitGroup
		files, err := ioutil.ReadDir(chn.Path)
		if err != nil {
			srv.Err = err
			return
		}
		for _, f := range files {
			if strings.HasSuffix(f.Name(), ".log") {
				handle, err := os.Open(chn.Path + f.Name())
				if err != nil {
					srv.Err = err
					return
				}
				var nr int
				lf := f
				sc := bufio.NewScanner(handle)
				linech := make(chan *IRCLine, 1024)
				donech := make(chan bool, 32)
				wg.Add(1)
				go func(lchn *IRCChannel, lsc *bufio.Scanner, lnr int) {
					for lsc.Scan() {
						lnr++
						linech <- &IRCLine{FilePath: lchn.Path + lf.Name(), LineNumber: nr, RawLine: lsc.Text()}
					}
					wg.Done()
					donech <- true
				}(chn, sc, nr)

				for i := 0; i < 32; i++ {
					wg.Add(1)
					go func(lchn *IRCChannel) {
						defer wg.Done()
						for {
							select {
							case line := <-linech:
								line, err := line.Parse(line.RawLine)
								if err == nil {
									lchn.Lines <- line
								}
							case <-donech:
								donech <- true
								return
							}
						}
					}(chn)
				}
				wg.Wait()
				handle.Close()
				close(linech)
				close(donech)
			}
		}
		chn.Done <- true
	}
	srv.Err = nil
}

// ParseLine takes one line and returns a fully populated IRCLine
func (il *IRCLine) Parse(line string) (*IRCLine, error) {
	arr := strings.Split(line, " ")
	if len(arr) < 2 {
		return nil, errors.New("ParseLine: line too short")
	}
	timefield := arr[0]
	nick := arr[1]
	if nick == "***" {
		return nil, errors.New("ParseLine: Line is a join/quit/topic change")
	}

	if nick == "*" {
		if len(arr) < 3 {
			return nil, errors.New("ParseLine: line too short")
		}
		nick = arr[2]
		il.IsAction = true
	}
	if !il.IsAction && !strings.HasPrefix(nick, "<") && !strings.HasSuffix(nick, ">") {
		return nil, errors.New("ParseLine: line is not action yet nick lacks < >")
	}

	il.User = strings.TrimSuffix(strings.TrimPrefix(nick, "<"), ">")
	if il.IsAction {
		if len(arr) < 4 {
			il.Message = ""
		} else {
			// TODO: fix this.
			il.Message = strings.Join(arr[3:], " ")
		}
	} else {
		il.Message = strings.Join((arr[2:]), " ")
	}

	t := strings.TrimSuffix(strings.TrimPrefix(timefield, "["), "]")
	tl := strings.Split(t, ":")

	hour, err := strconv.Atoi(tl[0])
	if err != nil {
		return il, errors.New("ParseLine: Failed to parse hour from " + tl[0] + " : " + err.Error())
	}

	minute, err := strconv.Atoi(tl[1])
	if err != nil {
		return il, errors.New("ParseLine: Failed to parse minute from " + tl[1] + " : " + err.Error())
	}

	second, err := strconv.Atoi(tl[2])
	if err != nil {
		return il, errors.New("ParseLine: Failed to parse second from " + tl[2] + " : " + err.Error())
	}

	_, fp := filepath.Split(il.FilePath)
	ts := strings.Split(fp, ".")
	dl := strings.Join(ts[:len(ts)-1], "")
	dateArr := strings.Split(dl, "-")

	year, err := strconv.Atoi(dateArr[0])
	if err != nil {
		return il, errors.New("ParseLine: Failed to parse year from " + dateArr[0] + " : " + err.Error())
	}

	month, err := strconv.Atoi(dateArr[1])
	if err != nil {
		return il, errors.New("ParseLine: Failed to parse month from " + dateArr[1] + " : " + err.Error())
	}

	day, err := strconv.Atoi(dateArr[2])
	if err != nil {
		return il, errors.New("ParseLine: Failed to parse day from " + dateArr[2] + " : " + err.Error())
	}

	il.Time = time.Date(year, time.Month(month), day, hour, minute, second, 0, time.UTC)
	il.Hash = fmt.Sprintf("%X", sha256.Sum256([]byte(line+strconv.FormatInt(int64(il.LineNumber), 10)+il.Time.Format("2006-01-02"))))

	return il, nil
}

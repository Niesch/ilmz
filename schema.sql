/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE ilmz;

# Dump of table bth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bth`;

CREATE TABLE `bth` (
  `id` binary(64) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'Hash sum of complete line. Heavily used for lazy deduplication.',
  `user` text NOT NULL COMMENT 'Who sent the message',
  `message` text NOT NULL COMMENT 'The body of the message',
  `channel` text NOT NULL COMMENT 'What channel the message was sent in.',
  `filename` text NOT NULL COMMENT 'The logfilename on disk where the line is to be found',
  `time` datetime NOT NULL COMMENT 'When the message was obtained',
  `action` tinyint(1) NOT NULL COMMENT 'Whether the message was an ACTION or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table btn
# ------------------------------------------------------------

DROP TABLE IF EXISTS `btn`;

CREATE TABLE `btn` (
  `id` binary(64) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'Hash sum of complete line. Heavily used for lazy deduplication.',
  `user` text NOT NULL COMMENT 'Who sent the message',
  `message` text NOT NULL COMMENT 'The body of the message',
  `channel` text NOT NULL COMMENT 'What channel the message was sent in.',
  `filename` text NOT NULL COMMENT 'The logfilename on disk where the line is to be found',
  `time` datetime NOT NULL COMMENT 'When the message was obtained',
  `action` tinyint(1) NOT NULL COMMENT 'Whether the message was an ACTION or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table freenode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `freenode`;

CREATE TABLE `freenode` (
  `id` binary(64) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'Hash sum of complete line. Heavily used for lazy deduplication.',
  `user` text NOT NULL COMMENT 'Who sent the message',
  `message` text NOT NULL COMMENT 'The body of the message',
  `channel` text NOT NULL COMMENT 'What channel the message was sent in.',
  `filename` text NOT NULL COMMENT 'The logfilename on disk where the line is to be found',
  `time` datetime NOT NULL COMMENT 'When the message was obtained',
  `action` tinyint(1) NOT NULL COMMENT 'Whether the message was an ACTION or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table privnet
# ------------------------------------------------------------

DROP TABLE IF EXISTS `privnet`;

CREATE TABLE `privnet` (
  `id` binary(64) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'Hash sum of complete line. Heavily used for lazy deduplication.',
  `user` text NOT NULL COMMENT 'Who sent the message',
  `message` text NOT NULL COMMENT 'The body of the message',
  `channel` text NOT NULL COMMENT 'What channel the message was sent in.',
  `filename` text NOT NULL COMMENT 'The logfilename on disk where the line is to be found',
  `time` datetime NOT NULL COMMENT 'When the message was obtained',
  `action` tinyint(1) NOT NULL COMMENT 'Whether the message was an ACTION or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rizon
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rizon`;

CREATE TABLE `rizon` (
  `id` binary(64) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'Hash sum of complete line. Heavily used for lazy deduplication.',
  `user` text NOT NULL COMMENT 'Who sent the message',
  `message` text NOT NULL COMMENT 'The body of the message',
  `channel` text NOT NULL COMMENT 'What channel the message was sent in.',
  `filename` text NOT NULL COMMENT 'The logfilename on disk where the line is to be found',
  `time` datetime NOT NULL COMMENT 'When the message was obtained',
  `action` tinyint(1) NOT NULL COMMENT 'Whether the message was an ACTION or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table swehack
# ------------------------------------------------------------

DROP TABLE IF EXISTS `swehack`;

CREATE TABLE `swehack` (
  `id` binary(64) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'Hash sum of complete line. Heavily used for lazy deduplication.',
  `user` text NOT NULL COMMENT 'Who sent the message',
  `message` text NOT NULL COMMENT 'The body of the message',
  `channel` text NOT NULL COMMENT 'What channel the message was sent in.',
  `filename` text NOT NULL COMMENT 'The logfilename on disk where the line is to be found',
  `time` datetime NOT NULL COMMENT 'When the message was obtained',
  `action` tinyint(1) NOT NULL COMMENT 'Whether the message was an ACTION or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table wille
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wille`;

CREATE TABLE `wille` (
  `id` binary(64) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'Hash sum of complete line. Heavily used for lazy deduplication.',
  `user` text NOT NULL COMMENT 'Who sent the message',
  `message` text NOT NULL COMMENT 'The body of the message',
  `channel` text NOT NULL COMMENT 'What channel the message was sent in.',
  `filename` text NOT NULL COMMENT 'The logfilename on disk where the line is to be found',
  `time` datetime NOT NULL COMMENT 'When the message was obtained',
  `action` tinyint(1) NOT NULL COMMENT 'Whether the message was an ACTION or not',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package walker

import (
	"gitlab.com/Niesch/ilmz/parser"
	"io/ioutil"
	"strings"
)

// GetServers takes the top directory of the log structure and returns a list of
// IRCServers that correspond to found servers.
func GetServers(logdir string) ([]*parser.IRCServer, error) {
	var servers []*parser.IRCServer
	dirs, err := ioutil.ReadDir(logdir)
	if err != nil {
		return servers, err
	}
	for _, server := range dirs {
		s := &parser.IRCServer{Name: server.Name(), Path: logdir + server.Name() + "/"}
		servers = append(servers, s)
	}
	return servers, err
}

// GetChannels takes one IRCServer and returns a list of channels belonging to that server.
func GetChannels(server *parser.IRCServer) ([]*parser.IRCChannel, error) {
	var channels []*parser.IRCChannel
	dirs, err := ioutil.ReadDir(server.Path)
	if err != nil {
		return channels, err
	}
	for _, channel := range dirs {
		c := &parser.IRCChannel{Name: channel.Name(), Path: server.Path + channel.Name() + "/", Lines: make(chan *parser.IRCLine, 1024), Done: make(chan bool, 32)}
		if !strings.HasPrefix(c.Name, "#") {
			c.IsQuery = true
		}
		channels = append(channels, c)
	}
	return channels, err
}
